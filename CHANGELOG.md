# Changelog

# 0.1.03

* small fixes for adapted CDS

# 0.1.0.2

* minor adaption in filepath assembling

# 0.1.0.1

* fixed rounding issue for tile/regions and associated image data sizes

# 0.1.0.0

* reworked slide interface to match new plugin mechanism of clinical data service
* fix native/default tile size bug

# 0.0.11.1

* adapted README.md
* set default value for tile size to native (-1)

# 0.0.9.0 - 0.0.11.0

* added env variable to set default tile extent
* update caching logic (cleanup evicted cache items)

# 0.0.8.0

* reworked thumbnail retrieval

# 0.0.7.0

* handle requests and responses between client and server async

# 0.0.6.0

* slide handles are not shared over multiple threads anymore
* each slide is initialized and managed by one thread
* worker sockets are cached with a sliding expiration time (default: 10 min)

# 0.0.5.0

* moved slide access sdk initialization from constructor to dedicated functions
* removed layer/level access from initialization

# 0.0.4.0

* fix slide thumbnail and implemented fallback

# 0.0.3.0

* minor performance improvments during slide initialization

# 0.0.2.0

* performance improvments
* uniformity of slide model values (equal to other wsi-service plugins)
* bugfixes and refactoring

# 0.0.1.0

* initial commit