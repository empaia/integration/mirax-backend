﻿using MiraxAccessDotNet472.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;

namespace MiraxAccessDotNet472.Utils
{
    public static class ImageUtils
    {
        public static byte[] ProcessAssociatedImage(MrxsAssociatedImage associatedImage, Int32 maxX, Int32 maxY, string imageFormat, Int32 imageQuality)
        {
            ImageFormat format = ToImageFormat(imageFormat) ?? throw new NotSupportedException(string.Format("Image format {0} not supported", imageFormat));
            using (MemoryStream msIn = new MemoryStream(associatedImage.Data))
            {
                msIn.Position = 0;
                using (Bitmap bmp = new Bitmap(msIn))
                {
                    Bitmap result = bmp;
                    if (bmp.Width > maxX || bmp.Height > maxY)
                    {
                        result = MakeThumbnail(bmp, maxX, maxY);
                    }
                    return ToEncodedByteArray(result, format, imageQuality);
                }
            }
        }

        public static Bitmap MakeThumbnail(Bitmap originalImage, int maxWidth, int maxHeight)
        {
            int newWidth, newHeight;

            if (originalImage.Width > originalImage.Height)
            {
                newWidth = maxWidth;
                newHeight = (Int32)Math.Round((float)originalImage.Height / originalImage.Width * maxWidth);
            }
            else
            {
                newWidth = (Int32)Math.Round((float)originalImage.Width / originalImage.Height * maxHeight);
                newHeight = maxHeight;
            }

            Bitmap resizedImage = new Bitmap(newWidth, newHeight);

            using (Graphics g = Graphics.FromImage(resizedImage))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.DrawImage(originalImage, 0, 0, newWidth, newHeight);
            }

            return resizedImage;
        }

        public static byte[] ProcessImageRegion(
            MrxsImageRegion region, Size requestedSize, string imageFormat, Int32 imageQuality, List<int> imageChannels, Color backgroundColor
        )
        {
            ImageFormat format = ToImageFormat(imageFormat) ?? throw new NotSupportedException(string.Format("Image format {0} not supported", imageFormat));
            if (region.IsEmptyRegion())
            {
                using (Bitmap bmp = CreateEmptyBitmap(requestedSize.Width, requestedSize.Height, backgroundColor))
                {
                    return ToEncodedByteArray(bmp, format, imageQuality);
                }
            }

            using (Bitmap bmp = ConvertUnknownPixelFormatByteImageTo32bppRgbBitmap(region))
            {
                Bitmap result = bmp;
                if (bmp.Width < requestedSize.Width || bmp.Height < requestedSize.Height)
                {
                    result = PasteRegionOnBackground(bmp, requestedSize.Width, requestedSize.Height, region.PasteCoordinates, backgroundColor);
                }
                if (imageChannels != null && imageChannels.Count > 0)
                {
                    result = ComposeChannels(result, imageChannels);

                }
                return ToEncodedByteArray(result, format, imageQuality);
            }
        }

        public static Bitmap ComposeChannels(Bitmap originalBitmap, List<int> selectedChannels)
        {
            int width = originalBitmap.Width;
            int height = originalBitmap.Height;

            Bitmap composedBitmap = new Bitmap(width, height);

            BitmapData originalData = originalBitmap.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            BitmapData composedData = composedBitmap.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);

            unsafe
            {
                byte* originalPointer = (byte*)originalData.Scan0;
                byte* composedPointer = (byte*)composedData.Scan0;

                int pixelSize = 4;

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        byte alpha = originalPointer[y * originalData.Stride + x * pixelSize + 3];

                        foreach (int channel in selectedChannels)
                        {
                            switch (channel)
                            {
                                case 0:
                                    composedPointer[y * composedData.Stride + x * pixelSize + 2] = alpha > 0 ? originalPointer[y * originalData.Stride + x * pixelSize + 2] : (byte)0;
                                    break;

                                case 1:
                                    composedPointer[y * composedData.Stride + x * pixelSize + 1] = alpha > 0 ? originalPointer[y * originalData.Stride + x * pixelSize + 1] : (byte)0;
                                    break;

                                case 2:
                                    composedPointer[y * composedData.Stride + x * pixelSize] = alpha > 0 ? originalPointer[y * originalData.Stride + x * pixelSize] : (byte)0;
                                    break;
                            }
                        }

                        composedPointer[y * composedData.Stride + x * pixelSize + 3] = alpha;
                    }
                }
            }

            originalBitmap.UnlockBits(originalData);
            composedBitmap.UnlockBits(composedData);

            return composedBitmap;
        }

        public static Bitmap PasteRegionOnBackground(Bitmap image, int newWidth, int newHeight, Point pasteCoordinates, Color backgroundColor)
        {
            Bitmap newBitmap = new Bitmap(newWidth, newHeight);
            using (Graphics g = Graphics.FromImage(newBitmap))
            {
                using (SolidBrush brush = new SolidBrush(backgroundColor))
                {
                    g.FillRectangle(brush, 0, 0, newBitmap.Width, newBitmap.Height);
                }

                g.DrawImage(image, pasteCoordinates);
            }

            return newBitmap;
        }

        private static byte[] ToEncodedByteArray(Bitmap bmp, ImageFormat format, int imageQuality)
        {
            using (MemoryStream msOut = new MemoryStream())
            {
                if (format.Equals(ImageFormat.Jpeg))
                {
                    ImageCodecInfo codecInfo = GetEncoderInfo(ImageFormat.Jpeg);
                    EncoderParameters enc_params = new EncoderParameters(1);
                    enc_params.Param[0] = new EncoderParameter(Encoder.Quality, (long)imageQuality);
                    bmp.Save(msOut, codecInfo, enc_params);
                }
                else
                {
                    bmp.Save(msOut, format);
                }
                return msOut.ToArray();
            }
        }

        public static ImageFormat ToImageFormat(string imageFormat)
        {
            if (imageFormat != null)
            {
                string imgF = imageFormat.ToLower();
                if (imgF == "jpeg" || imgF == "jpg")
                    return ImageFormat.Jpeg;
                if (imgF == "png")
                    return ImageFormat.Png;
                if (imgF == "gif")
                    return ImageFormat.Gif;
                if (imgF == "bmp")
                    return ImageFormat.Bmp;
                if (imgF == "tiff" || imgF == "tif")
                    return ImageFormat.Tiff;
            }
            return null;
        }

        public static ImageCodecInfo GetEncoderInfo(ImageFormat format)
        {
            return ImageCodecInfo.GetImageEncoders().ToList().Find(delegate (ImageCodecInfo codec)
            {
                return codec.FormatID == format.Guid;
            });
        }

        public static Bitmap CreateEmptyBitmap(int width, int height, Color backgroundColor)
        {
            Bitmap bitmap = new Bitmap(width, height);

            using (Graphics g = Graphics.FromImage(bitmap))
            {
                using (Brush brush = new SolidBrush(backgroundColor))
                {
                    g.FillRectangle(brush, 0, 0, width, height);
                }
            }

            return bitmap;
        }

        public static byte[] ImageToInMemoryStream(Image image, ImageFormat format)
        {
            using (var stream = new MemoryStream())
            {
                image.Save(stream, format);
                return stream.ToArray();
            }
        }

        public static void WriteImageByteArrayToFile(MrxsImageRegion imageRegion, string filename, ImageFormat format)
        {
            if (imageRegion.IsEmptyRegion())
            {
                throw new InvalidDataException("No image data given");
            }

            using (Bitmap bmp = ConvertUnknownPixelFormatByteImageTo32bppRgbBitmap(imageRegion))
            {
                bmp.Save(filename, format);
            }
        }

        public static void WriteByteArrayToFile(byte[] imageData, string filename, ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream(imageData))
            {
                ms.Position = 0;
                using (Bitmap bmp = new Bitmap(ms))
                {
                    bmp.Save(filename, format);
                }
            }
        }

        public static Size GetImageSize(byte[] imageData)
        {
            using (MemoryStream ms = new MemoryStream(imageData))
            {
                ms.Position = 0;
                using (Bitmap bmp = new Bitmap(ms))
                {
                    return new Size(bmp.Width, bmp.Height);
                }
            }
        }

        public static Bitmap ConvertUnknownPixelFormatByteImageTo32bppRgbBitmap(MrxsImageRegion imageRegion)
        {
            Bitmap result = new Bitmap(imageRegion.Width, imageRegion.Height, imageRegion.Format);

            Rectangle rect = new Rectangle(0, 0, imageRegion.Width, imageRegion.Height);
            BitmapData bmpData = result.LockBits(rect, ImageLockMode.WriteOnly, imageRegion.Format);
            bmpData.PixelFormat = PixelFormat.Format32bppRgb;

            Marshal.Copy(imageRegion.Data, 0, bmpData.Scan0, imageRegion.Data.Length);
            result.UnlockBits(bmpData);
            imageRegion.Format = PixelFormat.Format32bppRgb;
            return result;
        }

        public static Point GetPasteRegionCoordinates(int startX, int startY, int sizeX, int sizeY)
        {
            if ((startX + sizeX) < 0 || (startY + sizeY) < 0)
            {
                return new Point(0, 0); // rework
            }

            int pasteCoordX = 0;
            int pasteCoordY = 0;

            if (startX < 0)
            {
                pasteCoordX += Math.Abs(startX);
            }

            if (startY < 0)
            {
                pasteCoordY += Math.Abs(startY);
            }

            return new Point(pasteCoordX, pasteCoordY);
        }
    }
}
