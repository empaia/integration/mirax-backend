﻿using MiraxAccessDotNet472.Classes;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace MiraxAccessDotNet472.Utils
{
    public static class MetadataUtils
    {
        public static SlideLevel CalculateSlideLevel(SlideExtent baselayerSlideExtent, Int32 levelIndex, Int32 maxLevelCount)
        {
            float downsampleFactor = (float)Math.Pow(2, maxLevelCount - levelIndex);
            Int32 levelWidth = (Int32)(baselayerSlideExtent.X / downsampleFactor);
            Int32 levelHeight = (Int32)(baselayerSlideExtent.Y / downsampleFactor);
            // set z parameter to 1 by default (until z stack is supported
            return new SlideLevel(new SlideExtent(levelWidth, levelHeight, 1), downsampleFactor);
        }

        public static SlideExtent CalculateBaselayerSlideExtent(Dictionary<string, string> slideParams, Tuple<float, float> overlaps, SlideExtent nativeTileExtent)
        {
            Tuple<Int32, Int32> imageNumber = GetInt32PropertyXY(slideParams, "IMAGENUMBER");
            Int32 imageDivisions = GetImageDivisions(slideParams);

            Int32 overlappingTilesX = (imageNumber.Item1 / imageDivisions) - 1;
            Int32 overlappingTilesY = (imageNumber.Item2 / imageDivisions) - 1;

            Int32 partialWidthOverlapping = (Int32)(overlappingTilesX * Math.Floor(nativeTileExtent.X - overlaps.Item1));
            float partialWidthNonOverlapping = (imageNumber.Item1 - overlappingTilesX) * nativeTileExtent.X;
            Int32 levelWidth = (Int32)(partialWidthOverlapping + partialWidthNonOverlapping);

            Int32 partialHeightOverlapping = (Int32)(overlappingTilesY * Math.Floor(nativeTileExtent.Y - overlaps.Item2));
            float partialHeightNonOverlapping = (imageNumber.Item2 - overlappingTilesY) * nativeTileExtent.Y;
            Int32 levelHeight = (Int32)(partialHeightOverlapping + partialHeightNonOverlapping);

            return new SlideExtent(levelWidth, levelHeight, 1);
        }

        public static Tuple<float, float> GetFloatPropertyXY(Dictionary<string, string> paramDict, string propertyName)
        {
            float val1 = 0;
            float val2 = 0;
            bool hasValue = paramDict.TryGetValue(string.Format("{0}_X", propertyName), out string valueX);
            if (hasValue)
            {
                val1 = float.Parse(valueX, CultureInfo.InvariantCulture);
            }

            hasValue = paramDict.TryGetValue(string.Format("{0}_Y", propertyName), out string valueY);
            if (hasValue)
            {
                val2 = float.Parse(valueY, CultureInfo.InvariantCulture);
            }
            return Tuple.Create(val1, val2);
        }

        public static Tuple<Int32, Int32> GetInt32PropertyXY(Dictionary<string, string> paramDict, string propertyName)
        {
            Int32 val1 = 0;
            Int32 val2 = 0;
            bool hasValue = paramDict.TryGetValue(string.Format("{0}_X", propertyName), out string valueX);
            if (hasValue)
            {
                val1 = Int32.Parse(valueX, CultureInfo.InvariantCulture);
            }

            hasValue = paramDict.TryGetValue(string.Format("{0}_Y", propertyName), out string valueY);
            if (hasValue)
            {
                val2 = Int32.Parse(valueY, CultureInfo.InvariantCulture);
            }
            return Tuple.Create(val1, val2);
        }

        public static Int32 GetImageDivisions(Dictionary<string, string> paramDict)
        {
            Int32 val1 = 0;
            bool hasValue = paramDict.TryGetValue("CameraImageDivisionsPerSide", out string valueX);
            if (hasValue)
            {
                val1 = Int32.Parse(valueX, CultureInfo.InvariantCulture);
            }
            return val1;
        }

        public static SlideType GetSlideType(Dictionary<string, string> paramDict)
        {
            bool hasValue = paramDict.TryGetValue("SLIDE_TYPE", out string slideType);
            if (hasValue)
            {
                if (slideType.Equals(Constants.BRIGHTFIELD)) return SlideType.BRIGHTFIELD;
                if (slideType.Equals(Constants.FLUORESCENCE)) return SlideType.FLUORESCENCE;
            }
            return SlideType.UNDEFINED;
        }

        public static List<SlideChannel> GetSlideChannels(Dictionary<string, string> slideParamDict, List<MrxsLayerInstance> layers)
        {
            List<SlideChannel> slideChannels = new List<SlideChannel>();
            if (GetSlideType(slideParamDict) == SlideType.FLUORESCENCE)
            {
                MrxsLayerInstance layer = layers.Find(lay => lay.LayerName.Equals(Constants.SLIDE_FILTER_LAYER));
                for (int index = 0; index < layer.Levels.Count; index++)
                {
                    MrxsLevelInstance level = layer.Levels[index];
                    bool hasValue = level.LevelParameters.TryGetValue(Constants.FILTER_NAME, out string filterName);
                    if (!hasValue) { filterName = "Dummy"; }
                    hasValue = level.LevelParameters.TryGetValue(Constants.RED_COLOR_VALUE, out string redChannel);
                    if (!hasValue) { redChannel = "0"; }
                    hasValue = level.LevelParameters.TryGetValue(Constants.GREEN_COLOR_VALUE, out string greenChannel);
                    if (!hasValue) { greenChannel = "0"; }
                    hasValue = level.LevelParameters.TryGetValue(Constants.BLUE_COLOR_VALUE, out string blueChannel);
                    if (!hasValue) { blueChannel = "0"; }

                    if (hasValue)
                    {
                        int red = Convert.ToInt32(redChannel);
                        int green = Convert.ToInt32(greenChannel);
                        int blue = Convert.ToInt32(blueChannel);
                        slideChannels.Add(new SlideChannel(index, filterName, new SlideColor(red, green, blue, 0)));
                    }
                }
            }
            else
            {
                slideChannels.Add(new SlideChannel(0, "Red", new SlideColor(255, 0, 0, 0)));
                slideChannels.Add(new SlideChannel(1, "Green", new SlideColor(0, 255, 0, 0)));
                slideChannels.Add(new SlideChannel(2, "Blue", new SlideColor(0, 0, 255, 0)));
            }
            return slideChannels;
        }

        public static SlideLevel GetOptimalThumbnailLevel(SlideInfo slideInfo, Int32 maxX, Int32 maxY)
        {
            Int32 lowestLevel = slideInfo.Levels.Count - 1;
            SlideLevel optimalLevel = slideInfo.Levels[lowestLevel];
            for (int i = lowestLevel; i >= 0; i--)
            {
                if (slideInfo.Levels[i].Extent.X > maxX && slideInfo.Levels[i].Extent.Y > maxY)
                {
                    break;
                }
                optimalLevel = slideInfo.Levels[i];
            }
            return optimalLevel;
        }

        public static Dictionary<string, string> ConvertObjectsToDictionary(Object[,] objects)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            if (objects == null || objects.GetLength(1) == 0)
            {
                return result;
            }
            for (int x = 0; x < objects.GetLength(1); x += 1)
            {
                result.Add(objects[0, x].ToString(), objects[1, x].ToString());
            }
            return result;
        }
    }
}
