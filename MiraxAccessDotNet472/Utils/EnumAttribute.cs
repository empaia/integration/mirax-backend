﻿using System;
using System.Reflection;

namespace MiraxAccessDotNet472.Utils
{
    public class EnumValueAttribute : Attribute
    {
        public string EnumValue { get; protected set; }

        public EnumValueAttribute(string value)
        {
            this.EnumValue = value;
        }
    }

    public static class EnumAttributeHelper
    {
        public static string GetEnumValue(this Enum value)
        {
            Type type = value.GetType();
            FieldInfo fieldInfo = type.GetField(value.ToString());

            EnumValueAttribute[] attribs = fieldInfo.GetCustomAttributes(
                typeof(EnumValueAttribute), false) as EnumValueAttribute[];

            return attribs.Length > 0 ? attribs[0].EnumValue : null;
        }
    }
}
