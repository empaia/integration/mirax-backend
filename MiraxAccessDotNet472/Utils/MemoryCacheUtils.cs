﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection;
using System.Runtime.Caching;

namespace MiraxAccessDotNet472.Utils
{
    public class SlideWorkerCacheItem
    {
        public byte[] Identity { get; }
        public DateTime DateTime { get; set; }

        public SlideWorkerCacheItem(byte[] identity)
        {
            Identity = identity;
            DateTime = DateTime.Now;
        }
    }

    public class SlideWorkerCache
    {
        private readonly Dictionary<string, SlideWorkerCacheItem> ItemCache;
        private readonly int _maxSize;
        private readonly int _slidingExpirationTimeInSecs;

        public SlideWorkerCache(int maxSize = -1, int slidingExpirationTimeinSecs = 600)
        {
            ItemCache = new Dictionary<string, SlideWorkerCacheItem>();
            _maxSize = maxSize;
            _slidingExpirationTimeInSecs = slidingExpirationTimeinSecs;
        }


        public bool ContainsSlideWorker(string key)
        {
            return ItemCache.ContainsKey(key);
        }


        public void AddSlideWorker(string key, byte[] identity)
        {
            if (!ItemCache.ContainsKey(key))
            {
                ItemCache.Add(key, new SlideWorkerCacheItem(identity));
            }
        }

        public bool TryGetSlideWorker(string key, out byte[] identity)
        {
            identity = null;
            if (ItemCache.ContainsKey(key))
            {
                ItemCache[key].DateTime = DateTime.Now;
                identity = ItemCache[key].Identity;
                return true;
            }
            return false;
        }

        public List<SlideWorkerCacheItem> GetAndRemoveEvictingItemsFromCache()
        {
            DateTime now = DateTime.Now;

            List<SlideWorkerCacheItem> items = new List<SlideWorkerCacheItem>();
            List<string> removeFromCache = new List<string>();
            foreach (KeyValuePair<string, SlideWorkerCacheItem> item in ItemCache)
            {
                if (now.Subtract(item.Value.DateTime) > TimeSpan.FromSeconds(_slidingExpirationTimeInSecs))
                {
                    items.Add(item.Value);
                    removeFromCache.Add(item.Key);
                }
            }

            removeFromCache.ForEach(key => ItemCache.Remove(key));

            if (_maxSize > -1 && ItemCache.Count > _maxSize)
            {
                int itemsToDelete = ItemCache.Count - _maxSize;
                var oldestEntries = ItemCache.OrderBy(kvp => kvp.Value.DateTime).Take(itemsToDelete).ToList();

                foreach (KeyValuePair<string, SlideWorkerCacheItem> item in oldestEntries)
                {
                    items.Add(item.Value);
                    ItemCache.Remove(item.Key);
                }
            }

            return items;
        }
    }

    public static class MemoryCacheUtils
    {
        public static MemoryCache InitializeTTLCache(Int32 maxSizeInMegabytes)
        {
            NameValueCollection config = new NameValueCollection();
            if (maxSizeInMegabytes != 0)
            {
                config = new NameValueCollection {
                    { "pollingInterval", "00:05:00" },
                    { "physicalMemoryLimitPercentage", "0" },
                    { "cacheMemoryLimitMegabytes", maxSizeInMegabytes.ToString() }
                };
            }

            return new MemoryCache("SlideWorkerCache", config);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage(
            "Code Smell", "S3011:Reflection should not be used to increase accessibility of classes, methods, or fields",
            Justification = "Will only be called if verbose logging is turned on")
        ]
        public static long GetApproximateSize(MemoryCache cache)
        {
            var statsField = typeof(MemoryCache).GetField("_stats", BindingFlags.NonPublic | BindingFlags.Instance);
            var statsValue = statsField.GetValue(cache);
            var monitorField = statsValue.GetType().GetField("_cacheMemoryMonitor", BindingFlags.NonPublic | BindingFlags.Instance);
            var monitorValue = monitorField.GetValue(statsValue);
            var sizeField = monitorValue.GetType().GetField("_sizedRefMultiple", BindingFlags.NonPublic | BindingFlags.Instance);
            var sizeValue = sizeField.GetValue(monitorValue);
            var approxProp = sizeValue.GetType().GetProperty("ApproximateSize", BindingFlags.NonPublic | BindingFlags.Instance);
            return (long)approxProp.GetValue(sizeValue, null);
        }
    }
}
