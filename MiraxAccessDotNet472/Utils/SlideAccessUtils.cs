﻿using MiraxAccessDotNet472.Classes;
using MiraxAccessDotNet472.Models;
using SLIDEACLib;
using SLIDEIOLib;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;

namespace MiraxAccessDotNet472.Utils
{
    public static class SlideAccessUtils
    {
        public static SlideInfo GetSlideInfo(MrxsSlideStructure extSlideInfo)
        {
            MrxsLayerInstance zoomLayer = GetZoomLayer(extSlideInfo.Layers);
            MrxsLevelInstance baseLevel = zoomLayer.Levels[0];

            SlideExtent baselayerSlideExtent = MetadataUtils.CalculateBaselayerSlideExtent(extSlideInfo.SlideParams, baseLevel.GetOverlap(), baseLevel.GetNativeTileExtent());
            List<SlideLevel> levelList = zoomLayer.Levels
                .Select((levelInstance, index) => MetadataUtils.CalculateSlideLevel(baselayerSlideExtent, zoomLayer.Levels.Count - index, zoomLayer.Levels.Count))
                .ToList();

            SlideInfo slideInfo = new SlideInfo
            {
                ID = "slide_id", // dummy ID
                NumLevels = zoomLayer.Levels.Count,
                Levels = levelList,
                Extent = baselayerSlideExtent,
                TileExtent = baseLevel.GetTileExtent(),
                PixelSizeNm = baseLevel.GetPixelSizeNm(),
                ChannelDepth = 8, // TODO
                Channels = MetadataUtils.GetSlideChannels(extSlideInfo.SlideParams, extSlideInfo.Layers),
                Format = "3dhistech-mirax"
            };

            return slideInfo;
        }

        public static MrxsSlideStructure GetSlideData(SeSlideInOutClass seSlideInOut, int defaultTileExtent)
        {
            string slideName = seSlideInOut.SlideName;
            Object[,] slideOtherParams = (Object[,])seSlideInOut.SlideOtherParams;
            Dictionary<string, string> slideParams = MetadataUtils.ConvertObjectsToDictionary(slideOtherParams);

            seSlideInOut.LayerList(
                CSeSlideLayerType.seLayerTypeTree,
                out _,
                out object layerNames,
                out _,
                out object levelNames,
                out _,
                out _,
                out _
            );

            string[] sLayerNames = (string[])layerNames;
            List<MrxsLayerInstance> layers = sLayerNames
                .Select(sLayerName => GetMrxsLayerInstance(seSlideInOut, sLayerName, (string[,])levelNames, defaultTileExtent))
                .OfType<MrxsLayerInstance>()
                .ToList();

            return new MrxsSlideStructure(slideName, slideParams, layers);
        }

        private static MrxsLayerInstance GetMrxsLayerInstance(SeSlideInOutClass seSlideInOut, string sLayerName, string[,] levelNames, int default_tile_extent)
        {
            string levelPrefix = GetLevelPrefix(sLayerName);
            if (levelPrefix == null)
            {
                return null;
            }

            Object[,] layerOtherParams = (Object[,])seSlideInOut.LayerOtherParams[sLayerName];
            Dictionary<string, string> layerParams = MetadataUtils.ConvertObjectsToDictionary(layerOtherParams);

            List<MrxsLevelInstance> levels = levelNames
                .OfType<string>()
                .Where(l => l.StartsWith(levelPrefix))
                .Select(l => GetMrxsLevelInstance(seSlideInOut, sLayerName, l, default_tile_extent))
                .OfType<MrxsLevelInstance>()
                .ToList();

            return new MrxsLayerInstance(sLayerName, levels, layerParams);
        }

        private static string GetLevelPrefix(string sLayerName)
        {
            string levelPrefix = null;
            if (sLayerName.Equals(Constants.SLIDE_ZOOM_LAYER))
            {
                levelPrefix = Constants.ZOOM_LEVEL;
            }
            else if (sLayerName.Equals(Constants.SLIDE_FILTER_LAYER))
            {
                levelPrefix = Constants.FILTER_LEVEL;
            }
            else if (sLayerName.Equals(Constants.SLIDE_INFO_LAYER))
            {
                levelPrefix = Constants.INFO_LEVEL;
            }
            return levelPrefix;
        }

        private static MrxsLevelInstance GetMrxsLevelInstance(SeSlideInOutClass seSlideInOut, string sLayerName, string sLevelName, int defaultTileExtent)
        {
            try
            {
                Object[,] levelParams = (Object[,])seSlideInOut.LevelOtherParams[sLayerName, sLevelName];
                Dictionary<string, string> lParams = MetadataUtils.ConvertObjectsToDictionary(levelParams);

                return new MrxsLevelInstance(sLevelName, lParams, defaultTileExtent);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static MrxsAssociatedImage GetAssociatedImage(SeSlideInOutClass seSlideInOut, AssociatedImageType type, string layerName)
        {
            int tableID = seSlideInOut.AccessLayer(layerName, type.GetEnumValue(), CSeSlideDataAccessMode.seAccesModeReadOnly);
            bool isOpen = seSlideInOut.IsLayerOpened(tableID, out _);

            if (isOpen)
            {
                Int32 levelSize = seSlideInOut.PictureSize[0, 0, tableID];
                if (levelSize == 0)
                {
                    LoggingUtils.LogWarning(string.Format("No image data for requested image of type {0}", type), true);
                    return null;
                }

                byte[] buffer = new byte[levelSize];
                IntPtr unmanagedPointer = Marshal.AllocHGlobal(levelSize);
                seSlideInOut.GetPicture(0, 0, tableID, true, 0, null, 0, ref levelSize, 0, unmanagedPointer);
                Marshal.Copy(unmanagedPointer, buffer, 0, levelSize);
                Marshal.FreeHGlobal(unmanagedPointer);

                Size imageSize = ImageUtils.GetImageSize(buffer);
                return new MrxsAssociatedImage(type, imageSize.Width, imageSize.Height, buffer);
            }
            return null;
        }

        public static MrxsLayerInstance GetZoomLayer(List<MrxsLayerInstance> layers)
        {
            return layers.Find(lay => lay.LayerName.Equals(Constants.SLIDE_ZOOM_LAYER));
        }

        // #################################################################################################### //
        //                                                                                                      //
        //                                      Using SlideACLib.dll                                            //
        //                                                                                                      //
        // #################################################################################################### //


        public static MrxsImageRegion ReadRegionNative(ITDHImageProvider imageProvider, tagRECT destinationRect, tagSIZE sourceSize)
        {
            if (destinationRect.right <= destinationRect.left || destinationRect.bottom <= destinationRect.top)
            {
                return new MrxsImageRegion(0, 0, 0, PixelFormat.Format32bppRgb, new byte[0]);
            }

            TDHBitmapImage tDHBitmapImage = new TDHBitmapImage();

            try
            {
                imageProvider.CreateImage(ref destinationRect, ref sourceSize, false, tDHBitmapImage);

                IntPtr source = tDHBitmapImage.LockBits();

                Int32 bitmapByteSize = tDHBitmapImage.Stride * tDHBitmapImage.Height;
                byte[] rawImageData = new byte[bitmapByteSize];
                Marshal.Copy(source, rawImageData, 0, bitmapByteSize);

                return new MrxsImageRegion(tDHBitmapImage.Width, tDHBitmapImage.Height, tDHBitmapImage.Stride, (PixelFormat)tDHBitmapImage.PixelFormat, rawImageData);
            }
            catch (Exception ex)
            {
                string errorMsg = "Exception occured while reading native image region: {0}";

                if (ex.GetType().IsAssignableFrom(typeof(InsufficientMemoryException))
                    || ex.GetType().IsAssignableFrom(typeof(OutOfMemoryException))
                    || ex.GetType().IsAssignableFrom(typeof(COMException))
                )
                {
                    errorMsg = "Requested image region exceeds memory bounds: {0} \n Consider decreasing max width/height.";
                }

                LoggingUtils.LogError(errorMsg, ex);
                throw;
            }
        }
    }
}
