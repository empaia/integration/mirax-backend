﻿using MiraxAccessDotNet472.Classes;
using NetMQ;
using Newtonsoft.Json;
using System.Text;

namespace MiraxAccessDotNet472.Utils
{
    public static class NetMQUtils
    {
        public static void SendMultipartMessage<T>(NetMQSocket socket, string clientId, T messagePayload)
        {
            var payload = JsonConvert.SerializeObject(messagePayload);
            NetMQMessage msg = new NetMQMessage();
            msg.Append(Encoding.UTF8.GetBytes(clientId));
            msg.Append(payload);
            socket.SendMultipartMessage(msg);
        }

        public static void SendMultipartMessageWithBinaryData<T>(NetMQSocket socket, string clientId, T messagePayload, byte[] binaryData)
        {
            var payload = JsonConvert.SerializeObject(messagePayload);
            NetMQMessage msg = new NetMQMessage();
            msg.Append(Encoding.UTF8.GetBytes(clientId));
            msg.Append(payload);
            msg.Append(binaryData);
            socket.SendMultipartMessage(msg);
        }

        public static void SendErrorResponse(NetMQSocket socket, string clientId, NetMQApiErrorResponse response)
        {
            SendMultipartMessageWithBinaryData(socket, clientId, response, new byte[0]);
        }
    }
}
