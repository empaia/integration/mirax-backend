﻿using System;

namespace MiraxAccessDotNet472.Utils
{
    public static class LoggingUtils
    {
        public static void LogError(string errorMessage, Exception exception)
        {
            Console.WriteLine("ERROR | {0}: {1}", errorMessage, exception.Message);
            Console.WriteLine("        {0}", exception.StackTrace);
        }

        public static void LogWarning(string warningMessage, bool verboseLogging)
        {
            if (verboseLogging)
            {
                Console.WriteLine("WARN  | {0}", warningMessage);
            }
        }

        public static void LogInfo(string infoMessage, bool verboseLogging)
        {
            if (verboseLogging)
            {
                Console.WriteLine("INFO  | {0}", infoMessage);
            }

        }
    }
}
