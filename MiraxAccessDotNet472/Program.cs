﻿using MiraxAccessDotNet472.Classes;
using MiraxAccessDotNet472.Models;
using MiraxAccessDotNet472.Utils;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace MiraxAccessDotNet472
{
    internal class Program
    {
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                // CLI Tool (currently only used for debugging/testing)
                if (args.Length > 2)
                {
                    throw new ArgumentException("Missing arguments... (File and Output directory)");
                }

                string fileDirectory = args[0];
                string outputDirectory = args[1];
                //Console.WriteLine("Mirax file directory: {0}", fileDirectory);
                //Console.WriteLine("Output directory: {0}", outputDirectory);

                if (!Directory.Exists(outputDirectory))
                {
                    throw new FileNotFoundException(string.Format("Not a valid output directory {0}", outputDirectory));
                }

                MrxsSlideAccess slideAccess;
                try
                {
                    LoggingUtils.LogInfo("Initializing slide access instance", true);
                    slideAccess = MrxsSlideAccess.CreateNewMrxsSlideAccessInstance(fileDirectory, null, -1);
                }
                catch (Exception ex)
                {
                    LoggingUtils.LogError("Error occured while opening new slide handle", ex);
                    return;
                }

                // Retrieve slide info
                SlideInfo slideInfo = slideAccess.GetSlideInfo();
                Console.WriteLine(JsonConvert.SerializeObject(slideInfo));

                // Retrieve associated images
                //MrxsAssociatedImage associatedImage1 = slideAccess.GetThumbnailImage(1024, 1024);
                //ImageUtils.WriteImageByteArrayToFile(associatedImage1.Data, Path.Combine(outputDirectory, "test_associated_image.jpeg"), ImageFormat.Jpeg);
                //MrxsAssociatedImage associatedImage2 = slideAccess.GetAssociatedImage(AssociatedImageType.LABEL);
                //MrxsAssociatedImage associatedImage3 = slideAccess.GetAssociatedImage(AssociatedImageType.MACRO);
                //ImageUtils.WriteImageByteArrayToFile(associatedImage1.Data, Path.Combine(outputDirectory, "test_associated_image.jpeg"), ImageFormat.Jpeg);

                // Get tile
                Int32 levelTile = 8, tileX = 0, tileY = 0;

                Stopwatch sw = Stopwatch.StartNew();
                MrxsImageRegion imageData = slideAccess.ReadImageTile(levelTile, tileX, tileY);
                sw.Stop();
                Console.WriteLine(string.Format("Elapsed time ReadImageTile: {0}ms", sw.ElapsedMilliseconds));

                sw.Restart();
                byte[] rawImageData = ImageUtils.ProcessImageRegion(imageData, new Size(slideInfo.TileExtent.X, slideInfo.TileExtent.Y), "jpeg", 100, null, Color.White);
                sw.Stop();
                Console.WriteLine(string.Format("Elapsed time ProcessImageRegion: {0}ms", sw.ElapsedMilliseconds));

                ImageUtils.WriteByteArrayToFile(rawImageData, Path.Combine(outputDirectory, string.Format("tile_level_{0}_{1}_{2}.jpeg", levelTile, tileX, tileY)), ImageFormat.Jpeg);

                // Get region
                //Int32 levelRegion = 5, startX = 0, startY = 0, sizeX = 4096, sizeY = 4096;
                //MrxsImageRegion imageData2 = slideAccess.ReadImageRegion(levelRegion, startX, startY, sizeX, sizeY);
                //byte[] rawImageData = ImageUtils.ProcessImageRegion(imageData2, new Size(sizeX, sizeY), "jpeg", 100, null, Color.White);
                //ImageUtils.WriteByteArrayToFile(rawImageData, Path.Combine(outputDirectory, string.Format("tile_level_{0}_{1}_{2}.jpeg", levelTile, tileX, tileY)), ImageFormat.Jpeg);
            }
            else
            {
                try
                {
                    // Backend server
                    AppConfiguration appConfiguration = new AppConfiguration();
                    MrxsServer server = new MrxsServer(appConfiguration);
                    server.Run();
                }
                catch (Exception ex)
                {
                    LoggingUtils.LogError("Runtimer Error occured.", ex);
                }

            }
        }
    }
}
