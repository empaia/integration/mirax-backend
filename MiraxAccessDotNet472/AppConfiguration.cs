﻿using System;
using System.Configuration;

namespace MiraxAccessDotNet472
{
    public class AppConfiguration
    {
        public Int32 CacheTTLInSecs { get; }
        public Int32 CacheMaxSlideHandles { get; }
        public Int32 DefaultTileExtent { get; }
        public Int32 CacheInvalidationIntervalSecs { get; }
        public string BaseDataDirectory { get; }
        public bool VerboseLogging { get; }
        public AppConfiguration()
        {
            try
            {
                this.CacheTTLInSecs = GetInt32ConfigurationParamer("CPM_CACHE_TTL_SECS", ConfigurationManager.AppSettings["cache_ttl_secs"]);
                this.CacheMaxSlideHandles = GetInt32ConfigurationParamer("CPM_MAX_CACHE_SLIDE_HANDLES", ConfigurationManager.AppSettings["max_cache_slide_handles"]);
                this.DefaultTileExtent = GetInt32ConfigurationParamer("CPM_DEFAULT_TILE_EXTENT", ConfigurationManager.AppSettings["default_tile_extent"]);
                this.CacheInvalidationIntervalSecs = GetInt32ConfigurationParamer("CPM_CACHE_INVALIDATION_INTERVAL_SECS", ConfigurationManager.AppSettings["cache_invalidation_interval_secs"]);
                this.VerboseLogging = GetBoolConfigurationParamer("CPM_VERBOSE_LOGGING", ConfigurationManager.AppSettings["verbose_logging"]);

                string dataDir = Environment.GetEnvironmentVariable("CPM_DATA_DIR");
                if (dataDir == null)
                {
                    dataDir = ConfigurationManager.AppSettings["data_dir"];
                }
                this.BaseDataDirectory = dataDir;
            }
            catch (Exception ex0) when (ex0 is ConfigurationErrorsException)
            {
                Console.WriteLine("Failed reading configuration");
            }
            catch (Exception ex1) when (ex1 is ArgumentNullException || ex1 is FormatException || ex1 is OverflowException)
            {
                Console.WriteLine("Failed parsing configuration parameter");
            }
        }

        private Int32 GetInt32ConfigurationParamer(string parameterName, string defaultSetting)
        {
            string value = Environment.GetEnvironmentVariable(parameterName);
            if (value != null && Int32.TryParse(value, out Int32 result))
            {
                return result;
            }
            if (Int32.TryParse(defaultSetting, out Int32 resultDefault))
            {
                return resultDefault;
            }
            throw new ConfigurationErrorsException();
        }

        private bool GetBoolConfigurationParamer(string parameterName, string defaultSetting)
        {
            string value = Environment.GetEnvironmentVariable(parameterName);
            if (value != null && bool.TryParse(value, out bool result))
            {
                return result;
            }
            if (bool.TryParse(defaultSetting, out bool resultDefault))
            {
                return resultDefault;
            }
            throw new ConfigurationErrorsException();
        }
    }
}
