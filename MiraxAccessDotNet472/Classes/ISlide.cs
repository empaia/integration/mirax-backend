﻿using MiraxAccessDotNet472.Models;
using System;

namespace MiraxAccessDotNet472.Classes
{
    internal interface ISlide
    {
        SlideInfo GetSlideInfo();
        MrxsAssociatedImage GetAssociatedImage(AssociatedImageType imageType);
        MrxsImageRegion ReadImageTile(Int32 level, Int32 tileX, Int32 tileY);
        MrxsImageRegion ReadImageRegion(Int32 level, Int32 startX, Int32 startY, Int32 sizeX, Int32 sizeY);
    }
}
