﻿using MiraxAccessDotNet472.Classes;
using MiraxAccessDotNet472.Models;
using MiraxAccessDotNet472.Utils;
using NetMQ;
using NetMQ.Sockets;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiraxAccessDotNet472
{
    public class MrxsServer
    {
        private readonly AppConfiguration appConfiguration;

        public MrxsServer(AppConfiguration appConfiguration)
        {
            this.appConfiguration = appConfiguration;
        }

        public void Run()
        {
            try
            {
                using (NetMQRuntime runtime = new NetMQRuntime())
                {
                    using (RouterSocket incomingRouterSocket = new RouterSocket())
                    using (RouterSocket brokerSocket = new RouterSocket())
                    {
                        LoggingUtils.LogInfo("Parent Worker binding to router socket on tcp port 5556", this.appConfiguration.VerboseLogging);
                        string routerAddress = string.Format("tcp://*:{0}", 5556);
                        incomingRouterSocket.Bind(routerAddress);

                        LoggingUtils.LogInfo("Parent Worker binding to internal socket `inproc://mrxs-worker`", this.appConfiguration.VerboseLogging);
                        brokerSocket.Bind("inproc://mrxs-worker");

                        runtime.Run(RequestHandler(new object[] { incomingRouterSocket, brokerSocket }), ResponseHandler(new object[] { incomingRouterSocket, brokerSocket }));
                    }
                }

            }
            finally
            {
                NetMQConfig.Cleanup();
            }
        }

        private async Task RequestHandler(object state)
        {
            RouterSocket incomingRouterSocket = (RouterSocket)((object[])state)[0];
            RouterSocket brokerSocket = (RouterSocket)((object[])state)[1];

            LoggingUtils.LogInfo(
                    string.Format("Initializing worker cache with {0} max slide handles and ttl {1}s", this.appConfiguration.CacheMaxSlideHandles, this.appConfiguration.CacheTTLInSecs),
                    this.appConfiguration.VerboseLogging
                );
            SlideWorkerCache slideWorkerCache = new SlideWorkerCache(this.appConfiguration.CacheMaxSlideHandles, this.appConfiguration.CacheTTLInSecs);
            DateTime lastCacheInvalidation = DateTime.Now;

            while (true)
            {
                NetMQMessage message = await incomingRouterSocket.ReceiveMultipartMessageAsync();
                var identity = message[0].ConvertToString();
                var payload = message[1].ConvertToString();
                LoggingUtils.LogInfo(string.Format("Received message from {0}: {1}", identity, payload), this.appConfiguration.VerboseLogging);

                PluginRequest request = JsonConvert.DeserializeObject<PluginRequest>(payload);

                if (request.RequestType == "ALIVE")
                {
                    LoggingUtils.LogInfo("Mirax plugin is alive", this.appConfiguration.VerboseLogging);
                    NetMQUtils.SendMultipartMessage(incomingRouterSocket, identity, new AliveResponse());
                }
                else
                {

                    string filepath = request.Filepath;

                    if (!slideWorkerCache.TryGetSlideWorker(filepath, out byte[] workerIdentity))
                    {
                        LoggingUtils.LogInfo(string.Format("Cache miss for `{0}`! Filehandle is not yet open", filepath), this.appConfiguration.VerboseLogging);

                        string stringIdentity = string.Format("{0}_{1}", filepath, Guid.NewGuid());
                        workerIdentity = Encoding.UTF8.GetBytes(stringIdentity);
                        DealerSocket workerSocket = new DealerSocket();
                        workerSocket.Options.Identity = workerIdentity;
                        workerSocket.Connect("inproc://mrxs-worker");

                        Task _t = Task.Factory.StartNew(WorkerTask, new object[] { filepath, workerSocket });

                        slideWorkerCache.AddSlideWorker(filepath, workerIdentity);
                    }
                    LoggingUtils.LogInfo(
                        string.Format("Sending message to worker {0}: {1}", Encoding.UTF8.GetString(workerIdentity, 0, workerIdentity.Length), message.ToString()),
                        this.appConfiguration.VerboseLogging
                    );
                    brokerSocket.SendMoreFrame(workerIdentity).SendMultipartMessage(message);
                }

                if (DateTime.Now.Subtract(lastCacheInvalidation) > TimeSpan.FromSeconds(this.appConfiguration.CacheInvalidationIntervalSecs))
                {
                    LoggingUtils.LogInfo("Cleaning up cache...", this.appConfiguration.VerboseLogging);
                    NetMQMessage killProcessMsg = new NetMQMessage();
                    killProcessMsg.Append(Encoding.UTF8.GetBytes("SIGNAL"));
                    killProcessMsg.Append(Encoding.UTF8.GetBytes("KILL_PROCESS"));

                    List<SlideWorkerCacheItem> evictedItems = slideWorkerCache.GetAndRemoveEvictingItemsFromCache();
                    foreach (byte[] id in evictedItems.Select(item => item.Identity))
                    {
                        brokerSocket.SendMoreFrame(id).SendMultipartMessage(killProcessMsg);
                        LoggingUtils.LogInfo(
                            string.Format("Kill process signal sent to worker {0}", Encoding.UTF8.GetString(id, 0, id.Length)),
                            this.appConfiguration.VerboseLogging
                        );
                    }

                    lastCacheInvalidation = DateTime.Now; // reset timer
                }
            }
        }

        private async Task ResponseHandler(object state)
        {
            RouterSocket incomingRouterSocket = (RouterSocket)((object[])state)[0];
            RouterSocket brokerSocket = (RouterSocket)((object[])state)[1];

            while (true)
            {
                NetMQMessage workerMessage = await brokerSocket.ReceiveMultipartMessageAsync();
                workerMessage.Pop(); // remove empty frame

                // Forward the reply to the client
                LoggingUtils.LogInfo(string.Format("Forwarding message to client {0}", workerMessage[0].ConvertToString()), this.appConfiguration.VerboseLogging);
                incomingRouterSocket.SendMultipartMessage(workerMessage);
            }
        }

        private void WorkerTask(object state)
        {
            string filepath = (string)((object[])state)[0];
            DealerSocket workerSocket = (DealerSocket)((object[])state)[1];

            MrxsSlideAccess slideAccess = null;
            if (!filepath.EndsWith("mrxs"))
            {
                LoggingUtils.LogWarning("Not a MRXS file", this.appConfiguration.VerboseLogging);

            }
            else
            {
                try
                {
                    filepath = Path.ChangeExtension(string.Format("{0}/data/{1}", "Z:", filepath), null).Replace("/", "\\");
                    LoggingUtils.LogInfo(string.Format("Initializing slide access instance: {0}", filepath), true);
                    slideAccess = MrxsSlideAccess.CreateNewMrxsSlideAccessInstance(filepath, this.appConfiguration.BaseDataDirectory, this.appConfiguration.DefaultTileExtent);
                }
                catch (Exception ex)
                {
                    LoggingUtils.LogError("Error occured while opening new slide handle", ex);
                }
            }

            while (true)
            {
                NetMQMessage message = workerSocket.ReceiveMultipartMessage();

                LoggingUtils.LogInfo(
                    string.Format("Worker {0} received message: {1}", filepath, message.ToString()), this.appConfiguration.VerboseLogging
                );

                if (message.FrameCount == 2)
                {
                    var clientId = message[0].ConvertToString();
                    var recvPayload = message[1].ConvertToString();

                    if (slideAccess == null)
                    {
                        NetMQUtils.SendMultipartMessageWithBinaryData(
                            workerSocket, clientId,
                            new NetMQApiSuccessResponse<WsiFileInfo>(ResponseType.success, 200, new WsiFileInfo(false, "", "mirax", new List<StorageAddress>())),
                            new byte[0]
                        );
                        break;
                    }

                    if (clientId == "SIGNAL" && recvPayload == "KILL_PROCESS")
                    {
                        LoggingUtils.LogInfo(string.Format("Shutting down process: {0}", filepath), true);
                        break; // task is killed by signal (cache invalidation)
                    }

                    PluginRequest request = JsonConvert.DeserializeObject<PluginRequest>(recvPayload);


                    if (request.RequestType.Equals("CHECK_IS_SUPPORTED"))
                    {
                        try
                        {
                            WsiFileInfo wsiFileInfo = slideAccess.GetWsiFilInfo();
                            NetMQUtils.SendMultipartMessageWithBinaryData(
                                workerSocket, clientId, new NetMQApiSuccessResponse<WsiFileInfo>(ResponseType.success, 200, wsiFileInfo), new byte[0]
                            );
                            if (!wsiFileInfo.IsSupported)
                            {
                                break;
                            }
                        }
                        catch (Exception ex)
                        {
                            NetMQApiErrorResponse errorResp = new NetMQApiErrorResponse(ResponseType.error, 500, ex.Message);
                            NetMQUtils.SendErrorResponse(workerSocket, clientId, errorResp);
                            break;

                        }

                    }
                    else if (request.RequestType.Equals("SLIDE_INFO"))
                    {
                        try
                        {
                            NetMQApiSuccessResponse<SlideInfo> reponse = new NetMQApiSuccessResponse<SlideInfo>(ResponseType.success, 200, slideAccess.GetSlideInfo());
                            NetMQUtils.SendMultipartMessageWithBinaryData(workerSocket, clientId, reponse, new byte[0]);
                        }
                        catch (Exception ex)
                        {
                            LoggingUtils.LogError("Error occured while retrieving slide info", ex);
                            NetMQApiErrorResponse errorResp = new NetMQApiErrorResponse(ResponseType.error, 500, ex.Message);
                            NetMQUtils.SendErrorResponse(workerSocket, clientId, errorResp);
                        }
                    }
                    else if (request.RequestType.Equals("LABEL") || request.RequestType.Equals("MACRO"))
                    {
                        Enum.TryParse(request.RequestType, out AssociatedImageType type);
                        try
                        {
                            MrxsAssociatedImage associatedImage = slideAccess.GetAssociatedImage(type);
                            if (associatedImage.Exists)
                            {
                                ImageMetadata imageMetadata = new ImageMetadata(string.Format("image/{0}", request.ImageFormat));
                                NetMQApiSuccessResponse<ImageMetadata> response = new NetMQApiSuccessResponse<ImageMetadata>(ResponseType.success, 200, imageMetadata);
                                byte[] imageData = ImageUtils.ProcessAssociatedImage(associatedImage, request.MaxX, request.MaxY, request.ImageFormat, request.ImageQuality);
                                NetMQUtils.SendMultipartMessageWithBinaryData(workerSocket, clientId, response, imageData);
                            }
                            else
                            {
                                NetMQApiErrorResponse errorResp = new NetMQApiErrorResponse(
                                    ResponseType.error, 404, string.Format("Associated image of type {0} does not exist", type)
                                );
                                NetMQUtils.SendErrorResponse(workerSocket, clientId, errorResp);
                            }
                        }
                        catch (Exception ex)
                        {
                            LoggingUtils.LogError(string.Format("Error occured while retrieving associated image of type {0}", type), ex);
                            NetMQApiErrorResponse errorResp = new NetMQApiErrorResponse(ResponseType.error, 500, ex.Message);
                            NetMQUtils.SendErrorResponse(workerSocket, clientId, errorResp);
                        }
                    }
                    else if (request.RequestType.Equals("THUMBNAIL"))
                    {
                        try
                        {
                            MrxsAssociatedImage associatedImage = slideAccess.GetAssociatedImage(AssociatedImageType.THUMBNAIL);
                            if (associatedImage.Exists)
                            {
                                ImageMetadata imageMetadata = new ImageMetadata(string.Format("image/{0}", request.ImageFormat));
                                NetMQApiSuccessResponse<ImageMetadata> response = new NetMQApiSuccessResponse<ImageMetadata>(ResponseType.success, 200, imageMetadata);
                                byte[] imageData = ImageUtils.ProcessAssociatedImage(associatedImage, request.MaxX, request.MaxY, request.ImageFormat, request.ImageQuality);
                                NetMQUtils.SendMultipartMessageWithBinaryData(workerSocket, clientId, response, imageData);

                            }
                            else
                            {
                                // UNUSED!
                                // fallback to generated thumbnail from actual image data
                                //associatedImage = slideAccess.GetThumbnailImage(maxX, maxY);
                                NetMQApiErrorResponse errorResp = new NetMQApiErrorResponse(ResponseType.error, 404, "Associated image of type THUMBNAIL does not exist");
                                NetMQUtils.SendErrorResponse(workerSocket, clientId, errorResp);
                            }
                        }
                        catch (Exception ex)
                        {
                            LoggingUtils.LogError("Error occured while retrieving associated image of type thumbnail", ex);
                            NetMQApiErrorResponse errorResp = new NetMQApiErrorResponse(ResponseType.error, 500, ex.Message);
                            NetMQUtils.SendErrorResponse(workerSocket, clientId, errorResp);
                        }
                    }
                    else if (request.RequestType.Equals("TILE"))
                    {
                        try
                        {
                            Color backgroundColor = Color.White;
                            if (request.PaddingColor != null && request.PaddingColor.Count == 3)
                            {
                                backgroundColor = Color.FromArgb(red: request.PaddingColor[0], green: request.PaddingColor[1], request.PaddingColor[2]);
                            }

                            SlideInfo slideInfo = slideAccess.GetSlideInfo();
                            MrxsImageRegion tile = slideAccess.ReadImageTile(request.Level, request.TileX, request.TileY);

                            ImageMetadata imageMetadata = new ImageMetadata(string.Format("image/{0}", request.ImageFormat));
                            NetMQApiSuccessResponse<ImageMetadata> response = new NetMQApiSuccessResponse<ImageMetadata>(ResponseType.success, 200, imageMetadata);

                            byte[] imageData = ImageUtils.ProcessImageRegion(tile, new Size(slideInfo.TileExtent.X, slideInfo.TileExtent.Y), request.ImageFormat, request.ImageQuality, request.ImageChannels, backgroundColor);
                            NetMQUtils.SendMultipartMessageWithBinaryData(workerSocket, clientId, response, imageData);
                        }
                        catch (Exception ex)
                        {
                            LoggingUtils.LogError("Error occured while retrieving tile", ex);
                            NetMQApiErrorResponse errorResp = new NetMQApiErrorResponse(ResponseType.error, 500, ex.Message);
                            NetMQUtils.SendErrorResponse(workerSocket, clientId, errorResp);
                        }
                    }
                    else if (request.RequestType.Equals("REGION"))
                    {
                        try
                        {
                            //LoggingUtils.LogInfo(
                            //    string.Format("Retrieving region [level={0}, start_x={1}, start_y={2}, size_x={3}, size_y={4}]", request.Level, request.StartX, request.StartY, request.SizeX, request.SizeY),
                            //    this.appConfiguration.VerboseLogging
                            //);

                            Color backgroundColor = Color.White;
                            if (request.PaddingColor != null && request.PaddingColor.Count == 3)
                            {
                                backgroundColor = Color.FromArgb(red: request.PaddingColor[0], green: request.PaddingColor[1], request.PaddingColor[2]);
                            }

                            MrxsImageRegion region = slideAccess.ReadImageRegion(request.Level, request.StartX, request.StartY, request.SizeX, request.SizeY);
                            ImageMetadata imageMetadata = new ImageMetadata(string.Format("image/{0}", request.ImageFormat));
                            NetMQApiSuccessResponse<ImageMetadata> response = new NetMQApiSuccessResponse<ImageMetadata>(ResponseType.success, 200, imageMetadata);


                            byte[] imageData = ImageUtils.ProcessImageRegion(region, new Size(request.SizeX, request.SizeY), request.ImageFormat, request.ImageQuality, request.ImageChannels, backgroundColor);
                            NetMQUtils.SendMultipartMessageWithBinaryData(workerSocket, clientId, response, imageData);
                        }
                        catch (Exception ex)
                        {
                            LoggingUtils.LogError("Error occured while retrieving region", ex);
                            NetMQApiErrorResponse errorResp = new NetMQApiErrorResponse(ResponseType.error, 500, ex.Message);
                            NetMQUtils.SendErrorResponse(workerSocket, clientId, errorResp);
                        }
                    }
                }
                LoggingUtils.LogInfo(
                    string.Format("Worker {0} succesfully processed request: {1}", filepath, message.ToString()),
                    this.appConfiguration.VerboseLogging
                );
            }
            workerSocket.Disconnect("inproc://mrxs-worker");
            workerSocket.Dispose();
        }
    }
}
