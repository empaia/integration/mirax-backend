﻿using MiraxAccessDotNet472.Classes;
using MiraxAccessDotNet472.Models;
using MiraxAccessDotNet472.Utils;
using SLIDEACLib;
using SLIDEIOLib;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace MiraxAccessDotNet472
{
    public class MrxsSlideAccess : IDisposable, ISlide
    {
        private readonly int defaultTileExtent;

        private readonly string fileDirectory;


        private readonly string dataDir;

        // out

        private WsiFileInfo wsiFileInfo;

        private SlideInfo slideInfo;

        // internal

        private MrxsSlideStructure extSlideInfo;

        private ITDHImageProvider imageProvider;

        private Dictionary<AssociatedImageType, MrxsAssociatedImage> associatedImages = new Dictionary<AssociatedImageType, MrxsAssociatedImage>();


        private bool disposedValue;

        public static MrxsSlideAccess CreateNewMrxsSlideAccessInstance(string fileDirectory, string localDataDir, int defaultTileExtent)
        {
            MrxsSlideAccess slideAccessInstance = new MrxsSlideAccess(fileDirectory, localDataDir, defaultTileExtent);
            try
            {
                slideAccessInstance.InitWsiInfo();
                slideAccessInstance.InitSlideInfo();
                return slideAccessInstance;
            }
            catch (Exception ex)
            {
                LoggingUtils.LogError("Failed to load and instantiate slide instance.", ex);
                throw new FileLoadException("Failed to load and instantiate slide instance.");
            }
        }

        private MrxsSlideAccess(string fileDirectory, string dataDir, int defaultTileExtent)
        {
            this.fileDirectory = fileDirectory;
            this.dataDir = dataDir;
            this.defaultTileExtent = defaultTileExtent;

        }

        private void InitWsiInfo()
        {
            if (this.fileDirectory == null || !File.Exists(string.Format("{0}.mrxs", this.fileDirectory)))
            {
                this.wsiFileInfo = new WsiFileInfo(false, "", "mirax", new List<StorageAddress>());
                return;
            }
            if (!Directory.Exists(this.fileDirectory))
            {
                this.wsiFileInfo = new WsiFileInfo(false, "", "mirax", new List<StorageAddress>());
                return;
            }

            string adaptedMainFilepath = string.Format("{0}.mrxs", this.fileDirectory)
                .Replace("\\", "/")
                .Replace(string.Format("Z:{0}/", this.dataDir), "");
            List<StorageAddress> resultList = new List<StorageAddress>
            {
                new StorageAddress(true, adaptedMainFilepath)
            };

            foreach (string additionalFile in Directory.EnumerateFiles(this.fileDirectory))
            {
                string adaptedFilepath = additionalFile.Replace("\\", "/").Replace(string.Format("Z:{0}/", this.dataDir), "");
                resultList.Add(new StorageAddress(false, adaptedFilepath));
            }

            this.wsiFileInfo = new WsiFileInfo(true, "mirax", "mirax", resultList);
        }

        private void InitSlideInfo()
        {
            SeSlideInOutClass seSlideInOut = null;
            try
            {
                seSlideInOut = new SeSlideInOutClass();
                seSlideInOut.OpenSlide(fileDirectory, CSeSlideOpenMode.seOpenModeRead);
                this.extSlideInfo = SlideAccessUtils.GetSlideData(seSlideInOut, defaultTileExtent);
                this.slideInfo = SlideAccessUtils.GetSlideInfo(extSlideInfo);
            }
            catch (Exception ex)
            {
                LoggingUtils.LogInfo(string.Format("Error while opening and initializing slide: {0}", ex.Message), true);
                this.wsiFileInfo = new WsiFileInfo(false, "", "mirax", new List<StorageAddress>());
            }
            finally
            {
                seSlideInOut?.CloseSlide();
            }
        }

        private void InitializeImageProvider()
        {
            try
            {
                this.imageProvider = new TDHImageProvider();
                this.imageProvider.OpenSlide(fileDirectory);
            }
            catch (Exception ex)
            {
                LoggingUtils.LogError("Error while initializing image provider", ex);
                throw;
            }

            if (this.extSlideInfo != null && MetadataUtils.GetSlideType(this.extSlideInfo.SlideParams) == SlideType.FLUORESCENCE)
            {
                var histoFunction = this.imageProvider.HistogramFunctions;
                for (int i = 0; i < histoFunction.ChannelCount; i++)
                {
                    histoFunction.ApplyDefaultToChannel(i);
                }
            }
        }

        public SlideInfo GetSlideInfo()
        {
            if (this.slideInfo == null)
            {
                InitSlideInfo();
            }
            return this.slideInfo;
        }

        public WsiFileInfo GetWsiFilInfo()
        {
            if (this.wsiFileInfo == null)
            {
                InitWsiInfo();
            }
            return this.wsiFileInfo;
        }

        public MrxsAssociatedImage GetAssociatedImage(AssociatedImageType imageType)
        {
            if (this.associatedImages.TryGetValue(imageType, out MrxsAssociatedImage image))
            {
                return image;
            }

            SeSlideInOutClass seSlideInOut = null;
            try
            {
                seSlideInOut = new SeSlideInOutClass();
                seSlideInOut.OpenSlide(this.fileDirectory, CSeSlideOpenMode.seOpenModeRead);
                MrxsAssociatedImage img = SlideAccessUtils.GetAssociatedImage(seSlideInOut, imageType, Constants.SCAN_DATA_LAYER);
                this.associatedImages.Add(imageType, img);
                return img;
            }
            catch (Exception ex)
            {
                LoggingUtils.LogWarning(
                    string.Format("Failed to retrieve associated image ({0}): {1}", imageType.ToString(), ex.Message), true
                );
                MrxsAssociatedImage img = new MrxsAssociatedImage(imageType, 0, 0, new byte[0]);
                this.associatedImages.Add(imageType, img);
                return img;
            }
            finally
            {
                seSlideInOut?.CloseSlide();
            }
        }

        public MrxsAssociatedImage GetThumbnailImage(Int32 maxX, Int32 maxY)
        {
            if (this.imageProvider == null)
            {
                InitializeImageProvider();
            }

            SlideLevel optimalLevel = MetadataUtils.GetOptimalThumbnailLevel(this.GetSlideInfo(), maxX, maxY);

            tagRECT rect = new tagRECT
            {
                left = 0,
                top = 0,
                right = this.imageProvider.SlideWidth,
                bottom = this.imageProvider.SlideHeight
            };

            Int32 sizeX = optimalLevel.Extent.X >= optimalLevel.Extent.Y ? maxX : (Int32)((float)optimalLevel.Extent.X / (float)optimalLevel.Extent.Y * (float)maxX);
            Int32 sizeY = optimalLevel.Extent.Y > optimalLevel.Extent.X ? maxY : (Int32)((float)optimalLevel.Extent.Y / (float)optimalLevel.Extent.X * (float)maxY);
            tagSIZE size = new tagSIZE { cx = sizeX, cy = sizeY };

            MrxsImageRegion imageRegion = SlideAccessUtils.ReadRegionNative(this.imageProvider, rect, size);
            if (!imageRegion.IsEmptyRegion())
            {
                using (Bitmap bmp = ImageUtils.ConvertUnknownPixelFormatByteImageTo32bppRgbBitmap(imageRegion))
                {
                    imageRegion.Data = ImageUtils.ImageToInMemoryStream(bmp, ImageFormat.Bmp);
                }
            }
            return new MrxsAssociatedImage(AssociatedImageType.THUMBNAIL, imageRegion.Width, imageRegion.Height, imageRegion.Data);
        }

        public MrxsImageRegion ReadImageTile(Int32 level, Int32 tileX, Int32 tileY)
        {
            if (this.imageProvider == null)
            {
                InitializeImageProvider();
            }

            SlideInfo si = this.GetSlideInfo();
            float downsampleFactor = si.Levels[level].DownsampleFactor;
            float tileExtentBaselayerX = si.TileExtent.X * downsampleFactor;
            float tileExtentBaselayerY = si.TileExtent.Y * downsampleFactor;
            Int32 left = (Int32)(tileExtentBaselayerX * tileX);
            Int32 top = (Int32)(tileExtentBaselayerY * tileY);
            tagRECT rect = new tagRECT
            {
                left = Math.Max(left, 0),
                top = Math.Max(top, 0),
                right = (int)Math.Min(tileExtentBaselayerX + left, si.Extent.X),
                bottom = (int)Math.Min(tileExtentBaselayerY + top, si.Extent.Y),
            };
            Int32 cx = (Int32)Math.Round((rect.right - rect.left) / downsampleFactor);
            Int32 cy = (Int32)Math.Round((rect.bottom - rect.top) / downsampleFactor);
            tagSIZE size = new tagSIZE { cx = cx, cy = cy };

            MrxsImageRegion imageRegion = SlideAccessUtils.ReadRegionNative(this.imageProvider, rect, size);
            return imageRegion;
        }

        public MrxsImageRegion ReadImageRegion(Int32 level, Int32 startX, Int32 startY, Int32 sizeX, Int32 sizeY)
        {
            if (this.imageProvider == null)
            {
                InitializeImageProvider();
            }

            float downsampleFactor = this.GetSlideInfo().Levels[level].DownsampleFactor;
            Int32 left = Math.Max((Int32)(startX * downsampleFactor), 0);
            Int32 top = Math.Max((Int32)(startY * downsampleFactor), 0);
            tagRECT rect = new tagRECT
            {
                left = left,
                top = top,
                right = Math.Min((Int32)((sizeX * downsampleFactor) + startX), this.imageProvider.SlideWidth),
                bottom = Math.Min((Int32)((sizeY * downsampleFactor) + startY), this.imageProvider.SlideHeight)
            };
            Int32 cx = (Int32)Math.Round((rect.right - rect.left) / downsampleFactor);
            Int32 cy = (Int32)Math.Round((rect.bottom - rect.top) / downsampleFactor);
            tagSIZE size = new tagSIZE { cx = cx, cy = cy };

            Point pasteCoordinates = ImageUtils.GetPasteRegionCoordinates(startX, startY, sizeX, sizeY);

            MrxsImageRegion imageRegion = SlideAccessUtils.ReadRegionNative(this.imageProvider, rect, size);
            imageRegion.PasteCoordinates = pasteCoordinates;
            return imageRegion;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                this.slideInfo = null;
                this.associatedImages.Clear();
                this.associatedImages = null;

                disposedValue = true;
            }
        }


        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
