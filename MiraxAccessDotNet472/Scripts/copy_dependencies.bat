@echo off
set project_dir=%1
set target_dir=%2

set source_dir_syswow64=C:\Windows\SysWOW64

echo Copying Intel Integrated Performance Primitives Binaries...
for /f "delims=" %%f in ('dir /b /s "%source_dir_syswow64%\ipp*6.1.dll" "%source_dir_syswow64%\libiomp5md.dll"') do (
    echo Copying "%%~nxf"...
    copy "%%f" "%target_dir%\"
)

set source_dir_slideac=C:\Program Files (x86)\Common Files\3DHISTECH
set file_list=SlideIO.dll SlideAC.dll ImgCodecs.dll

echo Copying 3DHistech SlideAC Binaries...
for %%f in (%file_list%) do (
    if exist "%source_dir_slideac%\%%f" (
        echo Copying "%%f"...
        copy "%source_dir_slideac%\%%f" "%target_dir%\"
    ) else (
        echo File "%%f" not found in "%source_dir_slideac%".
    )
)

echo Copying registration entries from resources...
copy "%project_dir%\Resources\SlideAC.reg" "%target_dir%\"

echo Copied external dependencies to target destination.

pushd "%target_dir%"
set "target_parent_dir=%CD%\.."

set zip_file="%target_parent_dir%\mirax_backend.zip"
echo Creating zip file "%zip_file%" with external dependencies and compiled project...

if exist "%zip_file%" del "%zip_file%"

powershell Compress-Archive '%target_dir%\*'  -CompressionLevel Optimal -DestinationPath '%zip_file%'

echo Zip file created.