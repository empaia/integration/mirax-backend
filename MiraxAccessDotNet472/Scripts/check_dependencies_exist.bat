@echo off
set source_dir_syswow64=C:\Windows\SysWOW64
set file_list_ipp=libiomp5md.dll ippcore-6.1.dll ippcc-6.1.dll ippi-6.1.dll ipps-6.1.dll

set "error="
for %%f in (%file_list_ipp%) do (
    if not exist "%source_dir_syswow64%\%%f" (
        echo File "%%f" not found.
        set "error=true"
    )
)

set source_dir_slideac=C:\Program Files (x86)\Common Files\3DHISTECH
set file_list_ac=SlideIO.dll SlideAC.dll ImgCodecs.dll
for %%f in (%file_list_ac%) do (
    if not exist "%source_dir_slideac%\%%f" (
        echo File "%%f" not found.
        set "error=true"
    )
)

if defined error (
    echo At least one file not found.
    exit /b 1
) else (
    echo All necessary dependencies found.
    exit /b 0
)