﻿using MiraxAccessDotNet472.Classes;
using System;

namespace MiraxAccessDotNet472.Models
{
    public class MrxsAssociatedImage
    {
        public MrxsAssociatedImage(AssociatedImageType associatedImageType, Int32 width, Int32 height, byte[] data)
        {
            AssociatedImageType = associatedImageType;
            Width = width;
            Height = height;
            Data = data;
            Exists = true;
            if ((width == 0 && height == 0) || data.Length == 0)
            {
                Exists = false;
            }
        }

        public AssociatedImageType AssociatedImageType { get; }
        public bool Exists { get; }
        public Int32 Width { get; }
        public Int32 Height { get; }
        public byte[] Data { get; }
    }
}
