﻿using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace MiraxAccessDotNet472.Models
{
    public class MrxsImageRegion
    {
        public MrxsImageRegion(Int32 width, Int32 height, Int32 stride, PixelFormat format, byte[] data)
        {
            Width = width;
            Height = height;
            Stride = stride;
            Format = format;
            Data = data;
            PasteCoordinates = new Point(0, 0);
        }

        public Int32 Width { get; set; }
        public Int32 Height { get; set; }
        public Int32 Stride { get; set; }
        public PixelFormat Format { get; set; }
        public byte[] Data { get; set; }
        public Point PasteCoordinates { get; set; }

        public bool IsEmptyRegion()
        {
            return (Width == 0 && Height == 0);
        }
    }
}
