﻿using System.Collections.Generic;
using System.Text;

namespace MiraxAccessDotNet472.Classes
{
    public class MrxsLayerInstance
    {
        public string LayerName { get; set; }
        public List<MrxsLevelInstance> Levels { get; set; }
        public Dictionary<string, string> LayerParameters { get; set; }

        public MrxsLayerInstance(string layerName, List<MrxsLevelInstance> levels, Dictionary<string, string> parameters)
        {
            LayerName = layerName;
            Levels = levels;
            LayerParameters = parameters;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(string.Format("Layer name: {0}\n", LayerName));
            foreach (var level in Levels)
            {
                sb.Append(string.Format("'- Item: {0}\n", level.ToString()));
            }
            return sb.ToString();
        }
    }
}
