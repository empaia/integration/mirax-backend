﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;

namespace MiraxAccessDotNet472.Classes
{
    public class PluginRequest
    {
        [JsonProperty("req")]
        public string RequestType { get; set; }

        [JsonProperty("min_workers")]
        public string MinWorkers { get; set; }

        [JsonProperty("filepath")]
        public string Filepath { get; set; }

        [JsonProperty("level")]
        public int Level { get; set; }

        [JsonProperty("max_x")]
        public int MaxX { get; set; }

        [JsonProperty("max_y")]
        public int MaxY { get; set; }

        [JsonProperty("start_x")]
        public int StartX { get; set; }

        [JsonProperty("start_y")]
        public int StartY { get; set; }

        [JsonProperty("size_x")]
        public int SizeX { get; set; }

        [JsonProperty("size_y")]
        public int SizeY { get; set; }

        [JsonProperty("tile_x")]
        public int TileX { get; set; }

        [JsonProperty("tile_y")]
        public int TileY { get; set; }

        [JsonProperty("image_channels")]
        public List<int> ImageChannels { get; set; } = null;

        [JsonProperty("image_format")]
        public string ImageFormat { get; set; }

        [JsonProperty("image_quality")]
        public int ImageQuality { get; set; }

        [JsonProperty("z")]
        public int Z { get; set; }

        [JsonProperty("padding_color")]
        public List<int> PaddingColor { get; set; }

    }

    public class AliveResponse
    {
        public AliveResponse()
        {
            IsAlive = true;
        }

        [JsonProperty("is_alive")]
        public bool IsAlive { get; set; }
    }

    public class ImageMetadata
    {
        public ImageMetadata(string mediaType)
        {
            MediaType = mediaType;
        }

        [JsonProperty("media_type")]
        public string MediaType { get; set; }
    }

    public class StorageAddress
    {
        public StorageAddress(bool mainAddress, string address)
        {
            MainAddress = mainAddress;
            Address = address;
        }

        [JsonProperty("main_address")]
        public bool MainAddress { get; set; }
        [JsonProperty("address")]
        public string Address { get; set; }
    }

    public class WsiFileInfo
    {
        public WsiFileInfo(bool isSupported, string format, string plugin, List<StorageAddress> storageAddresses)
        {
            IsSupported = isSupported;
            Format = format;
            Plugin = plugin;
            StorageAddresses = storageAddresses;
        }

        [JsonProperty("is_supported")]
        public bool IsSupported { get; set; }
        [JsonProperty("format")]
        public string Format { get; set; }
        [JsonProperty("plugin")]
        public string Plugin { get; set; }
        [JsonProperty("storage_addresses")]
        public List<StorageAddress> StorageAddresses { get; set; }

    }

    public class SlideColor
    {
        public SlideColor(int r, int g, int b, int a)
        {
            R = r;
            G = g;
            B = b;
            A = a;
        }

        [JsonProperty("r")]
        public Int32 R { get; set; }
        [JsonProperty("g")]
        public Int32 G { get; set; }
        [JsonProperty("b")]
        public Int32 B { get; set; }
        [JsonProperty("a")]
        public Int32 A { get; set; }
    }
    public class SlideChannel
    {
        public SlideChannel(int id, string name, SlideColor color)
        {
            ID = id;
            Name = name;
            Color = color;
        }

        [JsonProperty("id")]
        public Int32 ID { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("color")]
        public SlideColor Color { get; set; }
    }

    public class SlideLevel
    {
        public SlideLevel(SlideExtent extent, float downsampleFactor)
        {
            Extent = extent;
            DownsampleFactor = downsampleFactor;
        }

        [JsonProperty("extent")]
        public SlideExtent Extent { get; set; }
        [JsonProperty("downsample_factor")]
        public float DownsampleFactor { get; set; }
    }

    public class SlideExtent
    {
        public SlideExtent(int x, int y, int z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        [JsonProperty("x")]
        public Int32 X { get; set; }
        [JsonProperty("y")]
        public Int32 Y { get; set; }
        [JsonProperty("z")]
        public Int32 Z { get; set; }
    }

    public class SlidePixelSizeNm
    {
        public SlidePixelSizeNm(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        [JsonProperty("x")]
        public float X { get; set; }
        [JsonProperty("y")]
        public float Y { get; set; }
        [JsonProperty("z")]
        public float Z { get; set; }
    }

    public class SlideInfo
    {
        [JsonProperty("id")]
        public string ID { get; set; }
        [JsonProperty("channels")]
        public List<SlideChannel> Channels { get; set; }
        [JsonProperty("channel_depth")]
        public Int32 ChannelDepth { get; set; }
        [JsonProperty("extent")]
        public SlideExtent Extent { get; set; }
        [JsonProperty("num_levels")]
        public Int32 NumLevels { get; set; }
        [JsonProperty("pixel_size_nm")]
        public SlidePixelSizeNm PixelSizeNm { get; set; }
        [JsonProperty("tile_extent")]
        public SlideExtent TileExtent { get; set; }
        [JsonProperty("levels")]
        public List<SlideLevel> Levels { get; set; }
        [JsonProperty("format")]
        public string Format { get; set; }
    }

    public class NetMQApiResponse
    {
        [JsonProperty("rep")]
        [JsonConverter(typeof(StringEnumConverter))]
        public ResponseType ResponseType { get; set; }
        [JsonProperty("status_code")]
        public Int32 StatusCode { get; set; }
    }

    public class NetMQApiErrorResponse : NetMQApiResponse
    {
        public NetMQApiErrorResponse(ResponseType responseType, int statusCode, string detail)
        {
            ResponseType = responseType;
            StatusCode = statusCode;
            Detail = detail;
        }

        public NetMQApiErrorResponse(ResponseType responseType, int statusCode)
        {
            ResponseType = responseType;
            StatusCode = statusCode;
            Detail = "";
        }

        [JsonProperty("detail")]
        public string Detail { get; set; }
    }

    public class NetMQApiSuccessResponse<T> : NetMQApiResponse
    {
        public NetMQApiSuccessResponse(ResponseType responseType, int statusCode, T content)
        {
            ResponseType = responseType;
            StatusCode = statusCode;
            Content = content;
        }

        [JsonProperty("content")]
        public T Content { get; set; }
    }
}
