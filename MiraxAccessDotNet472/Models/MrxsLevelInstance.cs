﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace MiraxAccessDotNet472.Classes
{
    public class MrxsLevelInstance
    {
        public string LevelName { get; set; }
        public Dictionary<string, string> LevelParameters { get; set; }

        private SlidePixelSizeNm _pixelSizeNm;
        private Tuple<float, float> _overlap;
        private SlideExtent _nativeSlideExtent;
        private SlideExtent _tileExtent;

        public MrxsLevelInstance(string levelName, Dictionary<string, string> parameters, int defaultTileSize = 256)
        {
            LevelName = levelName;
            LevelParameters = parameters;

            this.SetPixelSizeNm();
            this.SetOverlap();
            this.SetTileExtent(defaultTileSize);
        }

        public SlidePixelSizeNm GetPixelSizeNm()
        {
            return this._pixelSizeNm;
        }

        private void SetPixelSizeNm()
        {
            SlidePixelSizeNm pixelSize = new SlidePixelSizeNm(0, 0, 0);

            bool hasValue = LevelParameters.TryGetValue("MICROMETER_PER_PIXEL_X", out string mppX);
            if (hasValue)
            {
                pixelSize.X = float.Parse(mppX, CultureInfo.InvariantCulture) * 1000;
            }

            hasValue = LevelParameters.TryGetValue("MICROMETER_PER_PIXEL_Y", out string mppY);
            if (hasValue)
            {
                pixelSize.Y = float.Parse(mppY, CultureInfo.InvariantCulture) * 1000;
            }
            this._pixelSizeNm = pixelSize;
        }

        public Tuple<float, float> GetOverlap()
        {
            return this._overlap;
        }

        private void SetOverlap()
        {
            float overlapX = 0;
            float overlapY = 0;
            bool hasValue = LevelParameters.TryGetValue("OVERLAP_X", out string sOverlapX);
            if (hasValue)
            {
                overlapX = float.Parse(sOverlapX, CultureInfo.InvariantCulture);
            }

            hasValue = LevelParameters.TryGetValue("OVERLAP_X", out string sOverlapY);
            if (hasValue)
            {
                overlapY = float.Parse(sOverlapY, CultureInfo.InvariantCulture);
            }
            this._overlap = Tuple.Create(overlapX, overlapY);
        }

        public SlideExtent GetTileExtent()
        {
            return this._tileExtent;
        }

        public SlideExtent GetNativeTileExtent()
        {
            return this._nativeSlideExtent;
        }

        private void SetTileExtent(int defaultTileSize)
        {
            int tileSizeX = 0;
            int tileSizeY = 0;
            bool hasValue = LevelParameters.TryGetValue("DIGITIZER_WIDTH", out string digitizerWidth);
            if (hasValue)
            {
                tileSizeX = Int32.Parse(digitizerWidth);
            }

            hasValue = LevelParameters.TryGetValue("DIGITIZER_HEIGHT", out string digitizerHeight);
            if (hasValue)
            {
                tileSizeY = Int32.Parse(digitizerHeight);
            }
            this._nativeSlideExtent = new SlideExtent(tileSizeX, tileSizeY, 1); // set z parameter to 1 by default (until z stack is supported)
            this._tileExtent = defaultTileSize == -1 ?
                this._nativeSlideExtent : new SlideExtent(defaultTileSize, defaultTileSize, 1);
        }
    }
}
