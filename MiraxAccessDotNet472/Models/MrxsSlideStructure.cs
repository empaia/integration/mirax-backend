﻿using System.Collections.Generic;

namespace MiraxAccessDotNet472.Classes
{
    public class MrxsSlideStructure
    {
        public string SlideName { get; }
        public Dictionary<string, string> SlideParams { get; }
        public List<MrxsLayerInstance> Layers { get; }

        public MrxsSlideStructure(string slideName, Dictionary<string, string> slideParams, List<MrxsLayerInstance> layers)
        {
            SlideName = slideName;
            SlideParams = slideParams;
            Layers = layers;
        }
    }
}
