﻿using MiraxAccessDotNet472.Utils;

namespace MiraxAccessDotNet472.Classes
{
    public static class Constants
    {
        // Layer names
        public const string SLIDE_ZOOM_LAYER = "Slide zoom level";
        public const string SCAN_DATA_LAYER = "Scan data layer";
        public const string SLIDE_FILTER_LAYER = "Slide filter level";
        public const string SLIDE_INFO_LAYER = "Slide info level";

        // Level names
        public const string SCAN_MAP = "ScanDataLayer_ScanMap";
        public const string SLIDE_BARCODE = "ScanDataLayer_SlideBarcode";
        public const string SLIDE_PREVIEW = "ScanDataLayer_SlidePreview";
        public const string WHOLE_SLIDE = "ScanDataLayer_WholeSlide";
        public const string SLIDE_THUMBNAIL = "ScanDataLayer_SlideThumbnail";

        public const string INFO_LEVEL = "ScanInfoLayer";

        public const string ZOOM_LEVEL = "ZoomLevel";
        public const string ZOOM_LEVEL_BASE = "ZoomLevel_{0}";

        public const string FILTER_LEVEL = "FilterLevel";
        public const string FILTER_LEVEL_BASE = "FilterLevel_{0}";

        // Slide types
        public const string BRIGHTFIELD = "SLIDE_TYPE_BRIGHTFIELD";
        public const string FLUORESCENCE = "SLIDE_TYPE_FLUORESCENCE";

        // Filter
        public const string FILTER_NAME = "FILTER_NAME";
        public const string RED_COLOR_VALUE = "COLOR_R";
        public const string GREEN_COLOR_VALUE = "COLOR_G";
        public const string BLUE_COLOR_VALUE = "COLOR_B";
    }

    public enum SlideType
    {
        BRIGHTFIELD,
        FLUORESCENCE,
        UNDEFINED
    }

    public enum AssociatedImageType
    {
        [EnumValue(Constants.SLIDE_BARCODE)]
        LABEL,
        [EnumValue(Constants.SLIDE_THUMBNAIL)]
        MACRO,
        [EnumValue(Constants.SLIDE_PREVIEW)]
        THUMBNAIL
    }

    public enum ResponseType
    {
        success,
        error
    }
}
