# Mirax Access DotNet472

This project is a simple NetMQ-Socket to enable access to Whole Slide Images in the Mirax file format (*.mrxs) using the native slide access library of 3DHistech. The socket provides a basic interface to obtain meta information and image data of a slide.

## Getting started

In order to get started, the slide access library has to be installed on your local machine. This can be done via the offical 3DHistech `Downloads` section: [3DHistech Software Downloads](https://www.3dhistech.com/research/software-downloads/). Choose the `SlideViewer 2.5 (32-bit version)` and follow the install instructions. 

(Alternatively: Use the slide access installer from [Virtuelle Mikroskopie](https://virtuelle-mikroskopie.de/download/). Download `SlideACx86.exe` and follow the install instructions)

As this project is meant to run in a Linux/Wine environment, the documentation focuses on the `32bit/x86` version of the slide access library. 
**This implies that you will have to download the 32bit SlideAC / SlideViewer!**

Further dependencies:
* NetMQ with AsyncIO (via nuget)
* Newtonsoft.JSON (via nuget)

When the build process is started, a pre-build event will be triggered to check if the necessary dependencies have been installed and will eventually fail, if these `dll`s do not exist. For the `x86_Release` build the SlideAC-dependencies (usually stored under `C:\Program Files (x86)\Common Files\3DHISTECH`) and underlying Intel Integrated Performance Primitives (IPP; stored under `C:\Windows\SysWOW64`) are required. After building the project, a post-build event is triggered, copying all necessary third-party depencencies to our build directory and will subsequently bundle all files in a zip file. This zip file is required as backend for the [Wsi Service Plugin Mirax](https://gitlab.com/empaia/services/wsi-service-plugin-mirax) (see more details in the repository).

## Configuration

The application can be adjusted with the following configuration parameters:

* `tcp_router_port`: TCP port of the backend server socket (default: 5557)
* `cache_ttl_secs`: Sliding expiration time of cached slide handles in seconds (default: 10 min)
* `max_cache_slide_handles`: Max cache size for slide handle cache (default: 10)
* `default_tile_extent`: Defines the default extent of a tile (default: -1)
* `cache_invalidation_interval_secs`: Interval in which cache handles are checked for invalidation (default: 60 secs)
* `verbose_logging`: If enabled, info messages will be written to standard out

These parameters can also be set via environment:

```
WSPM_TCP_ROUTER_PORT=5557
WSPM_CACHE_TTL_SECS=600
WSPM_MAX_CACHE_SLIDE_HANDLES=5
WSPM_DEFAULT_TILE_EXTENT=256 # set to -1 to use native tile extent
WSPM_CACHE_INVALIDATION_INTERVAL_SECS=60
WSPM_VERBOSE_LOGGING=false
```
